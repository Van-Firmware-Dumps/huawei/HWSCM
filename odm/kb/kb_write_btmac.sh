#!/system/bin/sh

MACBT_PATH=/data/vendor/bluedroid/macbt
NODE_PATH=/sys/devices/platform/hwsw_kb/kb_init_nfc_info

if
	cat $MACBT_PATH | grep :
then
	echo "`cat $MACBT_PATH`" > $NODE_PATH
else
	sleep 5
	echo "`cat $MACBT_PATH`" > $NODE_PATH
fi
exit
